/**
 * \file AzertCli.cpp
 * \brief .cpp of AzertCli.h
 * \author MICHEL Dylan & JURET Enzo
 * \version 1.0
 * \date 13 december 2018
 *
 *
 */


#include <cstring>
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctype.h>
#include "AzertCli.h"
#include "connexion.h"


AzertCli *AzertCli::singleton = 0;

using namespace std;

// Accesseur au singleton
AzertCli *AzertCli::instance()
{
  if(!singleton)
  {
    singleton = new AzertCli();
  }

  return singleton;
}
//Constructeur de AzerCli
AzertCli::AzertCli():
  dureeRestante(100),
  duree(3600),
  format(0),
  descriUp(false),
  description("My playlist of " + std::to_string(duree) + "second !"),
  name("myPlaylist"),
  user("d.michel")
{
}
//Get de AzertCli
int AzertCli::getDureeRestante()
{
  return dureeRestante;
}
int AzertCli::getDuree()
{
  return duree;
}
string AzertCli::getFormat()
{
  if(format)
  {
    return "XSPF";
  }
  else
  {
    return "M3U";
  }
}
string AzertCli::getDescription()
{
  return description;
}
string AzertCli::getName()
{
  return name;
}
string AzertCli::getUser()
{
  return user;
}
bool AzertCli::getBoolDescri()
{
  return descriUp;
}

//Set de AzertCli
void AzertCli::setDureeRestante(int _dureeRestante)
{
  dureeRestante = _dureeRestante;
}
void AzertCli::setDuree(const char *_duree)
{
  bool truedigit = true;

  for(uint i = 0; i < strlen(_duree); i++)
  {
    if(!isdigit(_duree[i]))
    {
      truedigit = false;
    }
  }

  if(truedigit)
  {
    duree = std::atoi(_duree);

    if(!descriUp)
    {
      description = ("Ma playslist de " + std::to_string(duree) + "min");
    }
  }
  else
  {
    cerr << "La configuration de la saisi a échoué.. Remise à l'ancienne valeur : " << duree  << " ! "  << atoi(_duree) << "is digit ??????" << isdigit(std::atoi(_duree)) << endl;
  }
}
void AzertCli::setFormat(string _format)
{
  if(_format == "0")
  {
    format = 0;
  }
  else if(_format == "1")
  {
    format = 1;
  }
  else
  {
    cerr << "The input format is incorrect: Appli defaut M3U" << endl;
    format = 0;
  }
}
void AzertCli::setDescription(string _description)
{
  description = _description;
}
void AzertCli::setName(string _name)
{
  name = _name;
}
void AzertCli::setUser(string _user)
{
  user = _user;
}
void AzertCli::setBoolDescriT(bool _descriUp)
{
  descriUp = _descriUp;
}
string AzertCli::Defilist()
{
  AzertCli *singleton = AzertCli::instance();
  string leschemins;

  for(AzertCCM *teste : singleton->laList)
  {
    leschemins = leschemins + connexion(teste->requeteSql(), singleton->getUser(), singleton->getFormat());
  }

  siDureePasVide(leschemins);
  return leschemins;
}
void AzertCli::siDureePasVide(string &_leschemins)
{
  AzertCli *mon_singleton_CLI = AzertCli::instance();
  string unajout = connexion("select x from  ppe3.step7('*','*'," + to_string(mon_singleton_CLI->getDuree())   + "," + to_string(mon_singleton_CLI->getDureeRestante()) + ");", singleton->getUser(), singleton->getFormat());

  if(mon_singleton_CLI->getDureeRestante() != 0)
  {
    _leschemins += unajout;
  }
}

error_t AzertCli::run(int argc, char **argv)
{
  struct argp_option optionDuration =
  {
    .name = "duration",
    .key = 'd',
    .arg = "INTEGER",
    .flags = (ARGP_NO_ARGS, ARGP_NO_EXIT),
    .doc = "Used to define the duration of the playlist. Syntaxe '-d3600'. Default 3600 ",
    .group = 0
  };
  struct argp_option optionName =
  {
    .name = "name",
    .key = 'n',
    .arg = "STRING",
    .flags = 0,
    .doc = "Used to define the name of the playlist. Syntaxe '-nMyPlaylist'. Default myPlaylist",
    .group = 0
  };
  struct argp_option optionDescription =
  {
    .name = "description",
    .key = 'D',
    .arg = "STRING",
    .flags = 0,
    .doc = "Used to define the description of the playlist. Syntaxe '-D\"My awesome description !\". Default My playlist of %duree% ' second !",
    .group = 0
  };
  struct argp_option optionFormat =
  {
    .name = "format",
    .key = 'f',
    .arg = "0 for M3U or 1 for XSPF",
    .flags = 0,
    .doc = "Used to define the format of the playlist. Syntaxe '-f1'. Default 0",
    .group = 0
  };
  struct argp_option optionGenre =
  {
    .name = "genre",
    .key = 'g',
    .arg = "STRING=INTERGER",
    .flags = 0,
    .doc = "Filtring arg. Syntaxe '-gHip-Hop=40'",
    .group = 1
  };
  struct argp_option optionArtist =
  {
    .name = "artist",
    .key = 'a',
    .arg = "STRING=INTEGER",
    .flags = 0,
    .doc = "Filtring arg. Syntaxe '-aIllenium=40'",
    .group = 1
  };
  struct argp_option optionAlbum =
  {
    .name = "album",
    .key = 'A',
    .arg = "STRING=INTEGER",
    .flags = 0,
    .doc = "Filtring arg. Syntaxe '-AanAlbum=40'",
    .group = 1
  };
  struct argp_option optionTitle =
  {
    .name = "title",
    .key = 't',
    .arg = "STRING=INTERGER",
    .flags = 0,
    .doc = "Filtring arg. Syntaxe '-tAnTitle=40'.",
    .group = 1
  };
  /*
  No fonctionnal method
    struct argp_option superSQL =
    {
      .name = "superSQL",
      .key = 's',
      .arg = "non",
      .flags = 0,
      .doc = "SQL à la mains",
      .group = 0
    };
  */
  struct argp_option optionUser =
  {
    .name = "userDB",
    .key = 'u',
    .arg = "STRING",
    .flags = 0,
    .doc = "Define the database USER. Default d.michel",
    .group = 0
  };
  struct argp_option optionFinal =
  {
    .name = 0,
    .key = 0,
    .arg = 0,
    .flags = 0,
    .doc = 0,
    .group = 0
  };
  struct argp_option options[] =
  {
    optionDuration,
    optionName,
    optionDescription,
    optionFormat,
    optionGenre,
    optionArtist,
    optionAlbum,
    optionTitle,
    //superSQL,
    optionUser,
    optionFinal
  };
  struct argp mon_argp =
  {
    .options = options,
    .parser = AzertCli::parse_opt,
    .args_doc = 0,
    .doc = 0,
    .children = 0,
    .help_filter = 0,
    .argp_domain = 0,
  };
  return argp_parse(&mon_argp, argc, argv, 0, 0, 0);
}
/*
Méthode qui sert à definir ou se trouve le = dans l'option passé
_argbeopt : argc passé dans la ligne de commande
*/
int SUBmethodeSubOPTman(string _argbeopt, int afterEgale)
{
  AzertCli *mon_singleton_CLI = AzertCli::instance();
  char lePourcent[3];
  int a = 0;
  afterEgale++;

  if(afterEgale != 0)
  {
    int i = afterEgale;

    while(isdigit(_argbeopt[i]) || ((afterEgale + 3) != i))
    {
      lePourcent[a] = _argbeopt[i];
      a++;
      i++;
    }

    if(atoi(lePourcent) == 0)
    {
      if(mon_singleton_CLI->getDureeRestante() > 30)
      {
        mon_singleton_CLI->setDureeRestante(mon_singleton_CLI->getDureeRestante() - 30);
        return 30;
      }
      else
      {
        a = mon_singleton_CLI->getDureeRestante();
        mon_singleton_CLI->setDureeRestante(0);
      }
    }

    if(atoi(lePourcent) > 99)
    {
      a = mon_singleton_CLI->getDureeRestante();
      mon_singleton_CLI->setDureeRestante(0);
      return a;
    }

    if(mon_singleton_CLI->getDureeRestante() > (atoi(lePourcent)))
    {
      mon_singleton_CLI->setDureeRestante(mon_singleton_CLI->getDureeRestante() - atoi(lePourcent));
      return atoi(lePourcent);
    }
    else
    {
      a = mon_singleton_CLI->getDureeRestante();
      mon_singleton_CLI->setDureeRestante(mon_singleton_CLI->getDureeRestante() - mon_singleton_CLI->getDureeRestante());
      return a;
    }
  }
  else
  {
    mon_singleton_CLI->setDureeRestante(mon_singleton_CLI->getDureeRestante() - 30);
    return 30;
  }
}


int SUBwhereisEgale(string _argbeopt)
{
  for(uint i = 0; sizeof(_argbeopt) > i; i++)
  {
    if(_argbeopt[i] == '=')
    {
      return i;
    }
  }

  return -1;
}

string SUBsupprEgale(string _argbeopt, int whereisEgale)
{
  string sansEgale;

  if(whereisEgale != -1)
  {
    for(int  i = 0; i < whereisEgale ; i++)
    {
      sansEgale += _argbeopt[i];
    }

    return sansEgale;
  }
  else
  {
    return _argbeopt;
  }
}




void ecritureFichier()
{
  AzertCli *playlist = AzertCli::instance();
  string nameFile = playlist->getName() + "." + playlist->getFormat().c_str();

  if(playlist->getFormat() == "M3U")
  {
    ofstream fichier(nameFile.c_str(), ios::out | ios::trunc);
    string description_argu = "#" + playlist->getDescription();
    string lesPistes = playlist->Defilist();

    if(fichier)
    {
      fichier << description_argu << endl;
      fichier << lesPistes;
      fichier.close();
    }
    else
    {
      cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
  }
  else
  {
    ofstream fichier(nameFile.c_str(), ios::out | ios::trunc);
    string description_argu = "<!--" + playlist->getDescription() + "-->";
    string hautdepage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    string version = "1";
    string versionXSPF = "http://xspf.org/ns/0/";
    string lesPistes = playlist->Defilist();

    if(fichier)
    {
      fichier << description_argu << endl << hautdepage << endl;
      fichier << "<playlist version=\"" << version << "\"" << " xmlns=\"" << versionXSPF << "\">" << endl;
      fichier << "  <tracklist>" << endl;
      fichier << lesPistes;
      fichier << "  </tracklist>" << endl << "</playlist>";
      fichier.close();
    }
    else
    {
      cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
  }
}

int AzertCli::parse_opt(int key, char *arg, struct argp_state *state)
{
  AzertCli *mon_singleton_CLI = AzertCli::instance();
  string argbeopt;
  int charsubopts, whereisEgale;

  switch(key)
  {
    case 'd':
    {
      mon_singleton_CLI->setDuree(arg);
      state->arg_num = state->arg_num;
      break;
    }

    case 'n':
    {
      mon_singleton_CLI->setName(arg);
      break;
    }

    case 'D':
    {
      bool ok = true;
      mon_singleton_CLI->setDescription(arg);
      mon_singleton_CLI->setBoolDescriT(ok);
      break;
    }

    case 'f':
    {
      mon_singleton_CLI->setFormat(arg);
      break;
    }

    /*
          case 's':
          {
          break;
          }
    */

    case 'g':
    {
      argbeopt = arg;
      whereisEgale = SUBwhereisEgale(argbeopt);
      charsubopts = SUBmethodeSubOPTman(argbeopt, whereisEgale);
      argbeopt = SUBsupprEgale(argbeopt, whereisEgale);
      mon_singleton_CLI->laList.push_back(new Genre(argbeopt, charsubopts));
      break;
    }

    case 'a':
    {
      argbeopt = arg;
      whereisEgale = SUBwhereisEgale(argbeopt);
      charsubopts = SUBmethodeSubOPTman(argbeopt, whereisEgale);
      argbeopt = SUBsupprEgale(argbeopt, whereisEgale);
      mon_singleton_CLI->laList.push_back(new Artist(argbeopt, charsubopts));
      break;
    }

    case 'A':
    {
      argbeopt = arg;
      whereisEgale = SUBwhereisEgale(argbeopt);
      charsubopts = SUBmethodeSubOPTman(argbeopt, whereisEgale);
      argbeopt = SUBsupprEgale(argbeopt, whereisEgale);
      mon_singleton_CLI->laList.push_back(new Album(argbeopt, charsubopts));
      break;
    }

    case 't':
    {
      argbeopt = arg;
      whereisEgale = SUBwhereisEgale(argbeopt);
      charsubopts = SUBmethodeSubOPTman(argbeopt, whereisEgale);
      argbeopt = SUBsupprEgale(argbeopt, whereisEgale);
      mon_singleton_CLI->laList.push_back(new Title(argbeopt, charsubopts));
      break;
    }

    case 'u':
    {
      mon_singleton_CLI->setUser(arg);
    }
  }

  return 0;
}


//Get de AzertCCM
string AzertCCM::getleString()
{
  return leString;
}
int AzertCCM::getPercent()
{
  return percent;
}
//Set de AzertCCM
void AzertCCM::setleString(string _leString)
{
  leString = _leString;
}
void AzertCCM::setPercent(int _percent)
{
  percent = _percent;
}

//Les constructeurs
//AZERTCCM
AzertCCM::AzertCCM(string search, int perc)
{
  leString = search;
  percent = perc;
}

Genre::Genre(string search, int perc): AzertCCM(search, perc)
{
  leString = search;
  percent = perc;
}
Artist::Artist(string search, int perc): AzertCCM(search, perc)
{
  leString = search;
  percent = perc;
}
Album::Album(string search, int perc): AzertCCM(search, perc)
{
  leString = search;
  percent = perc;
}
Title::Title(string search, int perc): AzertCCM(search, perc)
{
  leString = search;
  percent = perc;
}

string Genre::requeteSql()
{
  AzertCli *mon_singleton_CLI = AzertCli::instance();
  return  "select x from  ppe3.step7('genre','" + leString +  "'," + to_string(mon_singleton_CLI->getDuree())   + "," + to_string(percent) + ");";
}

string Artist::requeteSql()
{
  AzertCli *mon_singleton_CLI = AzertCli::instance();
  return "select x from  ppe3.step7('artiste','" + leString +  "'," + to_string(mon_singleton_CLI->getDuree())   + "," + to_string(percent) + ");";
}

string Album::requeteSql()
{
  AzertCli *mon_singleton_CLI = AzertCli::instance();
  return "select x from  ppe3.step7('album','" + leString +  "'," + to_string(mon_singleton_CLI->getDuree())   + "," + to_string(percent) + ");";
}


string Title::requeteSql()
{
  AzertCli *mon_singleton_CLI = AzertCli::instance();
  return "select x from  ppe3.step7('titre','" + leString + "'," + to_string(mon_singleton_CLI->getDuree())   + "," + to_string(percent) + ");";
}

/**
 * Méthode qui ne retourne rien, elle sert à afficher le logo du logiciel
 * @author Zaz0
 *
 */
void AzertyMusicLogo()
{
  cout << "                         _         __  __           _       \n";
  cout << "     /\\                 | |       |  \\/  |         (_)      \n";
  cout << "    /  \\    _______ _ __| |_ _   _| \\  / |_   _ ___ _  ___  \n" ;
  cout << "   / /\\ \\  |_  / _ \\ '__| __| | | | |\\/| | | | / __| |/ __| \n" ;
  cout << "  / ____ \\  / /  __/ |  | |_| |_| | |  | | |_| \\__ \\ | (__  \n" ;
  cout << " /_/    \\_\\/___\\___|_|   \\__|\\__, |_|  |_|\\__,_|___/_|\\___| \n";
  cout << "                              __/ |                         \n";
  cout << "                             |___/                          \n";


  
  AzertCli *ma_CLI = AzertCli::instance();
  std::cout << "___________________________________________________________\n" << std::endl;
  std::cout << "Playlist name :\n    " << ma_CLI->getName() << std::endl;
  std::cout << "Playlist description :\n    " << ma_CLI->getDescription() << std::endl;
  std::cout << "Playlist duration :\n    " << ma_CLI->getDuree() << std::endl;
  std::cout << "Playlist format :\n    " << ma_CLI->getFormat() << std::endl;
  std::cout << "___________________________________________________________\n" << std::endl;
  for(AzertCCM* list : ma_CLI->laList)
  {
    std::cout << list->requeteSql() << std::endl;
  }
}
