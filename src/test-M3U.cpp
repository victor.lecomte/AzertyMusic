#include <iostream>
#include <string>
#include <fstream>
#include <list>

using namespace std;

int main()
{
  ofstream fichier("test.M3U", ios::out | ios::trunc);
  string description_argu = "Voici la playlist de vos rêves !";

  if(fichier)
  {
    string description = "#" + description_argu;
    fichier << description;
    list<string> laliste;
    laliste.push_back("Enzo");
    laliste.push_back("Florent");
    laliste.push_back("Alex");
    laliste.push_back("Dylan");
    laliste.push_back("Mathis");
    laliste.push_back("Hugo");
    laliste.push_back("Clement");
    laliste.push_back("Victor");
    laliste.push_back("Dilhan");

    for(string a : laliste)
    {
      fichier << a << endl;
    }

    fichier.close();
  }
  else
  {
    cerr << "Impossible d'ouvrir le fichier !" << endl;
  }

  return 0;
}

