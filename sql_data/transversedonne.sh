# transversedonne.sh
#@author MICHEL Dylan


pistes=$(psql -h postgresql.bts-malraux72.net -c "set schema 'ppe3'; SELECT * FROM ancien_morceau;")

debutprocedure()
{
    if [ ! $(ls morceau.sql | grep "Aucun fichier ou dossier de ce type") ]
    then
        rm morceau.sql
    fi
    cp morceau.sql.base morceau.sql 
}


trans()
{
    i=1

    cp "$1" temp.txt
    sed -i -e '1d' temp.txt
    while read sline
    do
        line=$(echo $sline | sed -e "s#\[#\\\[#g;s#\]#\\\]#g")
        sed -i -e "s#\t$line\t#\t$i\t#g" morceau.sql
        
        i=$(($i + 1))
    done < temp.txt
    rm temp.txt
}
decoupageAlbArt()
{
    rm "$1".sql
    i=1
    if [ "$1" = "album_morceau" ]
    then
        nb="\$2"
        echo "Album morceau"
        echo "COPY ppe3.album_morceau (id_album, id_morceau) FROM stdin;" >> "$1".sql
    elif [ "$1" = "artiste_morceau" ]
    then
        nb="\$3"
        echo "Artiste morceau"
        echo "COPY ppe3.artiste_morceau (id_artiste, id_morceau) FROM stdin;" >> "$1".sql
    fi
    
    cp morceau.sql temp.sql
    
    sed -i -e '1d' temp.sql
    while read sline
    do
        if [ $(echo "$sline" | awk -F "\t" '{print '$nb'}') ]
        then
            echo "$sline" | awk -F "\t" '{print '$nb' "\t'$i'"}' >> "$1".sql
        else
            echo "la ligne $i est vide."
        fi
        
        i=$(($i+1))
    done < temp.sql
    
    rm temp.sql
}
deleteAlbArtInMorceau()
{
    cp morceau.sql temp.sql
    sed -n 1p morceau.sql.base | sed -e "s#album, artiste, ##g;s#fk_sousgenre, ##g" > morceau.sql
    sed -i -e '1d' temp.sql
    
    while read sline
    do
        echo "$sline" | awk -F "\t" '{print $1 "\t" $4 "\t" $6 "\t" $7 "\t" $8 "\t" $9}' >> morceau.sql
    done < temp.sql
    rm temp.sql
}

nettoyagebdd()
{
    psql -h postgresql.bts-malraux72.net -c "\i ~/AzertyMusic/sql_data/rebase.sql" 
}

copytoinsert()
{
    psql -h postgresql.bts-malraux72.net -c "delete from ppe3.$1 *;"
    psql -h postgresql.bts-malraux72.net -c "\i ~/AzertyMusic/sql_data/$1.sql;"
}

debutprocedure

trans "album.sql" "2"
trans "artiste.sql" "3"
trans "genre.sql" "4"
trans "format.sql" "7"

decoupageAlbArt "album_morceau"
decoupageAlbArt "artiste_morceau"

deleteAlbArtInMorceau

nettoyagebdd
copytoinsert "album"
copytoinsert "artiste"
copytoinsert "format"
copytoinsert "genre"
copytoinsert "polyphonie"
copytoinsert "morceau"
copytoinsert "album_morceau"
copytoinsert "artiste_morceau"
